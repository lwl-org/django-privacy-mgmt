# Changelog

## Unreleased

## 0.2.4 (2024-12-04)

- hide unnecessary paragraph

## 0.2.3 (2024-09-09)

- add translations for fr, nl, ru

## 0.2.2 (2024-08-06)

- fix translations (delete the empty ``en/django.mo``)
- fix migration problems with languages

## 0.2.1 (2024-07-30)

- update translations
- configure `djlint`
- format `*.html` files with `djlint`

## 0.2.0 (2024-07-23)

- Add support for Django 4.0
- Replace usage of `ugettext_lazy`
- Use python 3 `__str__` instead of `__unicode__` and `force_str` its values

## 0.1.1 (2024-06-19)

- Fix display of essential cookie descriptions
- Fix interactable area of cookie description toggle button

## 0.1.0 (2024-06-07)

- Improved overall features
  - Extend privacy model
  - Event-based updates
  - Extend api
  - Versioning of cookie consent
  - Improve styling
- Add `django-solo` dependency
- Update german translation
- Remove *.egg-info from repository

## 0.0.24

- Add translations to tracking items initialization, disabled statistics by default

## 0.0.23

- Add privacy settings model and a manual migration to populate the settings and tracking items

## 0.0.22

- Add additional two categories: "Youtube Videos" and "Google Maps"

## 0.0.21

- Add optional translatable fields description, lifetime, cookie

## 0.0.20

- Fix French translation

## 0.0.19

- Add French translation
- Blocktrans tags are now trimmed for better po file readability

## 0.0.18

- Add missing migration
- Improve docs

## 0.0.17

- Improve documentation
- Add support for Django 2
- Remove `null has no effect on ManyToManyField` warning

## 0.0.16

- Improve documentations

## 0.0.15

- Add german translations for most of the strings

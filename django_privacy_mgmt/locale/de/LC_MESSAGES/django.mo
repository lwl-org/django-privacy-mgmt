��            )   �      �  .  �  
   �     �     �                7     M     a     z     �  "   �     �  !   �          0     P     j     p     �  '   �     �     �     �  
   �     �  #   �          5  M  P  n  �     	     	  &   4	     [	     o	     �	     �	     �	  $   �	     �	  $   
     9
  *   W
  #   �
  $   �
     �
  
   �
     �
       %     
   >     I     U     q       (   �     �     �                                                                                 
                                  	                      <a href="#" class="btn btn-default btn-xs js-cookie-accept pull-right">Accept</a> This website uses cookies to help ensure that you enjoy the best experience. <br /> If you do not consent to our use of cookies, please adjust your <a href="#" class="js-cookie-settings">privacy settings</a> accordingly. Accept all Category Essential Category Essential Description Category Marketing Category Miscellaneous Category Social Media Category Statistics Category essential title Category marketing description Category marketing title Category miscellaneous description Category miscellaneous title Category social media description Category social media title Category statistics description Category statistics title Close Cookies in general Description Domain of the cookie (e.g. .domain.com) Lifetime Ordering Privacy Settings Reject all Save selection The cookie's lifetime (e.g. 1 Year) What's the cookie's purpose? What's the item's purpose? Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-0730 09:38+0200
Last-Translator: Peter Wischer <peter.wischer@lwl.org>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 <a href="#" class="btn btn-default btn-xs js-cookie-accept pull-right">Akzeptieren</a> Diese Website verwendet Cookies, um sicherzustellen, dass Sie das beste Erlebnis geniessen können. <br /> Wenn Sie mit der Verwendung von Cookies nicht einverstanden sind, passen Sie bitte Ihre <a href="#" class="js-cookie-settings">Datenschutzeinstellungen</a> entsprechend an. Alle akzeptieren Kategorie Wesentliche Beschreibung der Kategorie Wesentliche Kategorie Marketing Kategorie Sonstiges Kategorie Sozialen Medien Kategorie Statistik Titel der Kategorie Wesentliche Beschreibung der Kategorie Marketing Titel der Kategorie Marketing Beschreibung der Kategorie Sonstiges Titel der Kategorie Sonstiges Beschreibung der Kategorie Sozialen Medien Titel der Kategorie Sozialen Medien Beschreibung der Kategorie Statistik Titel der Kategorie Statistik Schließen Cookies im Allgemeinen Beschreibung Domain des Cookies (z.B. .domain.com) Lebenszeit Reihenfolge Privatsphäre-Einstellungen Alle ablehnen Auswahl speichern Die Lebenszeit des Cookies (z.B. 1 Jahr) Wozu dient der Cookie? Wofür ist das item da? 
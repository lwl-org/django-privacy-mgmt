��    
      l      �       �   .  �   
         +     1     D     I     R  
   c     n  A  }  ]  �          +     2     G     N     W     u     �           	                           
       <a href="#" class="btn btn-default btn-xs js-cookie-accept pull-right">Accept</a> This website uses cookies to help ensure that you enjoy the best experience. <br /> If you do not consent to our use of cookies, please adjust your <a href="#" class="js-cookie-settings">privacy settings</a> accordingly. Accept all Close Cookies in general Name Ordering Privacy Settings Reject all Save selection Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 <a href="#" class="btn btn-default btn-xs js-cookie-accept pull-right">Accepter</a> Ce site Web utilise des cookies pour offrir une meilleure expérience de navigation aux utilisateurs. <br /> Si vous ne souhaitez pas que les cookies soient activés, adapte S.V.P. les <a href="#" class="js-cookie-settings">réglages de protection des données</a>. Tout accepter Fermer Cookies en général Le nom Commande Gestion de la sphère privée Tout refuser Enregistrer la sélection 
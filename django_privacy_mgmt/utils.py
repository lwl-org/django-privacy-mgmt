from hashlib import shake_256

from django.conf import settings

from django_privacy_mgmt.models import TrackingItem


def privacy_items_version_hash():
    tracking_items = [str(item.pk) for item in TrackingItem.objects.filter(site=settings.SITE_ID).order_by('pk')]
    version_hash = shake_256(','.join(tracking_items).encode('utf-8')).hexdigest(8)
    return version_hash

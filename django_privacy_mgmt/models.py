# -*- coding: utf-8 -*-
from django.contrib.sites.models import Site
from django.db import models
from django.utils.encoding import force_str
from django.utils.translation import gettext_lazy as _
from parler.models import TranslatableModel, TranslatedFields
from solo.models import SingletonModel

TRACKING_ITEM_CHOICE_ESSENTIALS = 'ESSENTIAL'
TRACKING_ITEM_CHOICE_STATISTICS = 'STATISTICS'
TRACKING_ITEM_CHOICE_MARKETING = 'MARKETING'
TRACKING_ITEM_CHOICE_SOCIAL_MEDIA = 'SOCIAL_MEDIA'
TRACKING_ITEM_CHOICE_MISCELLANEOUS = 'MISCELLANEOUS'

TRACKING_ITEM_CHOICES = (
    (TRACKING_ITEM_CHOICE_ESSENTIALS, 'Essential'),
    (TRACKING_ITEM_CHOICE_STATISTICS, 'Statistics'),
    (TRACKING_ITEM_CHOICE_MARKETING, 'Marketing'),
    (TRACKING_ITEM_CHOICE_SOCIAL_MEDIA, 'Social Media'),
    (TRACKING_ITEM_CHOICE_MISCELLANEOUS, 'Miscellaneous'),
)


class TrackingItem(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(
            _('Name'),
            max_length=1024,
            blank=True,
        ),
        slug=models.SlugField(
            _('Slug'),
            max_length=100,
            blank=True,
        ),
        description=models.TextField(
            _('Description'),
            blank=True,
            null=True,
            help_text=_("What's the item's purpose?"),
        ),
    )

    category = models.CharField(
        default=TRACKING_ITEM_CHOICE_ESSENTIALS,
        choices=TRACKING_ITEM_CHOICES,
        max_length=1024,
        null=False,
        blank=False,
    )

    ordering = models.IntegerField(_('Ordering'), default=0)
    # needs to be optional for x-site entries
    site = models.ManyToManyField(
        Site,
        blank=True,
    )

    def __str__(self):
        return force_str(self.name)

    class Meta:
        ordering = ('ordering',)

    def data(self):
        return {
            'name': self.name,
            'slug': self.slug,
            'id': self.pk,
            'description': self.description,
            'category': self.category,
        }


class TrackingCookie(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(_('Name'), max_length=1024, blank=True),
        description=models.TextField(
            _('Description'),
            blank=True,
            null=True,
            help_text=_("What's the cookie's purpose?"),
        ),
        lifetime=models.CharField(
            _('Lifetime'),
            max_length=255,
            blank=True,
            null=True,
            help_text=_("The cookie's lifetime (e.g. 1 Year)"),
        ),
        domain=models.CharField(
            _('Domain'),
            max_length=1024,
            blank=True,
            null=True,
            help_text=_('Domain of the cookie (e.g. .domain.com)'),
        ),
    )

    ordering = models.IntegerField(_('Ordering'), default=0)

    item = models.ForeignKey(TrackingItem, related_name='cookies', on_delete=models.CASCADE)

    def __str__(self):
        return force_str(self.name)

    class Meta:
        ordering = ('ordering',)

    def data(self):
        return {
            'name': self.name,
            'description': self.description,
            'lifetime': self.lifetime,
        }


class PrivacySettings(SingletonModel, TranslatableModel):
    translations = TranslatedFields(
        modal_description=models.TextField(
            _('Description'),
            blank=True,
            null=True,
        ),
        # category essential
        category_essential_title=models.CharField(
            _('Category essential title'),
            max_length=1024,
            blank=True,
            default='',
        ),
        category_essential_description=models.TextField(
            _('Category Essential Description'),
            blank=True,
            default='',
        ),
        # category statistics
        category_statistics_title=models.CharField(
            _('Category statistics title'),
            max_length=1024,
            blank=True,
            default='',
        ),
        category_statistics_description=models.TextField(
            _('Category statistics description'),
            blank=True,
            default='',
        ),
        # category marketing
        category_marketing_title=models.CharField(
            _('Category marketing title'),
            max_length=1024,
            blank=True,
            default='',
        ),
        category_marketing_description=models.TextField(
            _('Category marketing description'),
            blank=True,
            default='',
        ),
        # category social media
        category_social_media_title=models.CharField(
            _('Category social media title'),
            max_length=1024,
            blank=True,
            default='',
        ),
        category_social_media_description=models.TextField(
            _('Category social media description'),
            blank=True,
            default='',
        ),
        # category miscellaneous
        category_miscellaneous_title=models.CharField(
            _('Category miscellaneous title'),
            max_length=1024,
            default='',
        ),
        category_miscellaneous_description=models.TextField(
            _('Category miscellaneous description'),
            blank=True,
            default='',
        ),
    )

    def get_category_title(self, category):
        return getattr(self, 'category_{}_title'.format(category.lower()))

    def get_category_description(self, category):
        return getattr(self, 'category_{}_description'.format(category.lower()))

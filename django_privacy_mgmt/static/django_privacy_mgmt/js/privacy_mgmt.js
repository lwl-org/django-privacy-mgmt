/**
 * Privacy Management API
 *
 * This API is used to manage the privacy settings of a website.
 */
class PrivacyMgmt {
  constructor() {
    this.tracking_items = [];
    this.listeners = {
      change: [],
    };
  }

  PREFERENCE_COOKIE_NAME = "django_privacy_mgmt_preferences";

  CATEGORIES = {
    ESSENTIAL: {
      required: true,
      default: true,
    },
    STATISTICS: {
      required: false,
      default: false,
    },
    MARKETING: {
      required: false,
      default: false,
    },
    SOCIAL_MEDIA: {
      required: false,
      default: false,
    },
    MISCELLANEOUS: {
      required: false,
      default: false,
    },
  };

  initModal() {
    const privacyModal = $("#privacysettings");

    privacyModal.on("show.bs.modal", (e) => {
      const checkboxes = document.querySelectorAll(
        'input[data-js="privacy-check"]',
      );
      if (checkboxes) {
        checkboxes.forEach((checkbox) => {
          const name = checkbox.getAttribute("name");
          const category = checkbox.getAttribute("data-category");
          checkbox.checked =
            this.getPreference(name) || category === "ESSENTIAL";
        });
      }
    });

    const saveButton = document.querySelector(
      '[data-js="privacy-save-settings"]',
    );
    if (saveButton) {
      saveButton.addEventListener("click", (e) => {
        e.preventDefault();
        const checkboxes = document.querySelectorAll(
          'input[data-js="privacy-check"]',
        );
        const preferences = {};
        checkboxes.forEach((checkbox) => {
          const name = checkbox.getAttribute("name");
          preferences[name] = checkbox.checked;
        });
        this.setPreferences(preferences);

        privacyModal.modal("hide");
      });
    }

    const infoToggles = document.querySelectorAll(
      '[data-js="privacy-info-toggle"]',
    );
    if (infoToggles) {
      infoToggles.forEach((infoToggle) => {
        infoToggle.addEventListener("click", (e) => {
          e.preventDefault();
          const target = infoToggle.getAttribute("data-target");
          const info = document.getElementById(target);
          info.style.maxHeight =
            info.style.maxHeight && info.style.maxHeight !== "0px"
              ? "0px"
              : info.scrollHeight + "px";
          info.parentNode.classList.toggle("privacy-item--show");
        });
      });
    }

    const categoryToggles = document.querySelectorAll(
      'input[data-js="privacy-check"]',
    );
    if (categoryToggles) {
      categoryToggles.forEach((categoryToggle) => {
        categoryToggle.addEventListener("change", (e) => {
          // toggle all checkboxes in the category
          const category = e.target.getAttribute("name");
          if (this.CATEGORIES.hasOwnProperty(category)) {
            const categoryCheckboxes = document.querySelectorAll(
              `input[data-category="${category}"]`,
            );
            categoryCheckboxes.forEach((checkbox) => {
              checkbox.checked = e.target.checked;
            });
          }
        });
      });
    }

    return privacyModal;
  }

  initTrackingItems() {
    this.tracking_items = JSON.parse(
      document.getElementById("tracking_items").textContent,
    );
  }

  init() {
    this.initTrackingItems();

    const privacyModal = this.initModal();

    const settingsButton = document.querySelector(
      '[data-js="privacy-settings"]',
    );
    if (settingsButton) {
      settingsButton.addEventListener("click", (e) => {
        e.preventDefault();
        this.showSettings();
      });
    }

    const acceptAllButton = document.querySelector(
      '[data-js="privacy-accept-all"]',
    );
    if (acceptAllButton) {
      acceptAllButton.addEventListener("click", (e) => {
        e.preventDefault();
        this.setAcceptAll();
        privacyModal.modal("hide");
      });
    }

    const rejectAllButton = document.querySelector(
      '[data-js="privacy-reject-all"]',
    );
    if (rejectAllButton) {
      rejectAllButton.addEventListener("click", (e) => {
        e.preventDefault();
        this.setRejectAll();
        privacyModal.modal("hide");
      });
    }
  }

  setPreference(name, value) {
    const preferences = this._getPreferences();
    preferences[name] = value;

    const tracking_item = this.tracking_items.find(
      (item) => item.slug === name,
    );
    if (tracking_item) {
      preferences[tracking_item.id] = value;
    }

    if (this.CATEGORIES.hasOwnProperty(name)) {
      // If it's a category, set all items in the category
      for (const item of this.tracking_items) {
        if (item.category === name) {
          preferences[item.id] = value;
        }
      }
    }

    this.setPreferences(preferences);
  }

  setPreferences(preferences) {
    const oldPreferences = this._getPreferences();

    Cookies.set(this.PREFERENCE_COOKIE_NAME, JSON.stringify(preferences), {
      expires: 180,
    });
    this.dispatchEvent("change", preferences);

    // If preferences were removed, reload the page
    for (const key in oldPreferences) {
      if (oldPreferences[key] && !preferences[key]) {
        location.reload();
        break;
      }
    }
  }

  setAcceptAll() {
    const preferences = {};
    for (const category in this.CATEGORIES) {
      preferences[category] = true;
    }
    for (const item of this.tracking_items) {
      preferences[item.id] = true;
    }
    this.setPreferences(preferences);
  }

  setRejectAll() {
    const preferences = {};
    this.setPreferences(preferences);
  }

  showSettings() {
    $("#privacysettings").modal("show");
  }

  _getPreferences() {
    let preferences = Cookies.get(this.PREFERENCE_COOKIE_NAME);
    try {
      preferences = JSON.parse(preferences);
    } catch (e) {
      preferences = {};
    }
    return preferences;
  }

  getPreference(name) {
    this.initTrackingItems(); // Just to be sure
    const preferences = this._getPreferences();

    if (preferences.hasOwnProperty(name)) {
      return Boolean(preferences[name]);
    } else {
      // Is slug of a tracking item
      const tracking_item = this.tracking_items.find(
        (item) => item.slug === name,
      );
      if (tracking_item && preferences.hasOwnProperty(tracking_item.id)) {
        return Boolean(preferences[tracking_item.id]);
      }

      // Is category
      if (this.CATEGORIES.hasOwnProperty(name)) {
        return this.CATEGORIES[name]["default"];
      } else {
        return false;
      }
    }
  }

  addListener(eventName, callback, name = null) {
    // Ignore invalid names
    if (this.listeners.hasOwnProperty(eventName)) {
      this.listeners[eventName].push({
        callback: callback,
        name: name,
      });
    }
  }

  dispatchEvent(eventName, preferences) {
    // Ignore invalid names
    for (let listener of this.listeners[eventName]) {
      if (listener.name) {
        const name = listener.name;
        if (preferences.hasOwnProperty(name)) {
          listener.callback(preferences[name]);
        }
        // Is slug of a tracking item
        const tracking_item = this.tracking_items.find(
          (item) => item.slug === name,
        );

        if (tracking_item && preferences.hasOwnProperty(tracking_item.id)) {
          listener.callback(preferences[tracking_item.id]);
        }
      } else {
        listener.callback(preferences);
      }
    }
  }
}

window.django_privacy_mgmt = new PrivacyMgmt();
window.addEventListener("DOMContentLoaded", (event) => {
  window.django_privacy_mgmt.init();
});

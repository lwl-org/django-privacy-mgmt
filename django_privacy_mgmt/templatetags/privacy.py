# -*- coding: utf-8 -*-
from collections import defaultdict

from django import template
from django.conf import settings
from django.utils.safestring import mark_safe
from sekizai.data import UniqueSequence

from ..models import (
    TRACKING_ITEM_CHOICE_ESSENTIALS,
    TRACKING_ITEM_CHOICE_MARKETING,
    TRACKING_ITEM_CHOICE_MISCELLANEOUS,
    TRACKING_ITEM_CHOICE_SOCIAL_MEDIA,
    TRACKING_ITEM_CHOICE_STATISTICS,
    TRACKING_ITEM_CHOICES,
    PrivacySettings,
    TrackingItem,
)

register = template.Library()


@register.inclusion_tag('django_privacy_mgmt/popup.html', takes_context=True)
def render_privacy_settings_modal(context):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')
    tracking_items = TrackingItem.objects.filter(site=settings.SITE_ID)
    privacy_settings = PrivacySettings.get_solo()

    selectable_items = []
    for category, category_name in TRACKING_ITEM_CHOICES:
        filtered_items = tracking_items.filter(category=category)
        if not filtered_items.exists():
            continue

        selectable_items.append(
            {
                'category': category,
                'name': privacy_settings.get_category_title(category),
                'description': mark_safe(privacy_settings.get_category_description(category)),
                'items': filtered_items,
            }
        )

    return {
        'selectable_items': selectable_items,
        'privacy_settings': privacy_settings,
        sezikai_ctx_var: context.get(sezikai_ctx_var, defaultdict(UniqueSequence)),
    }


@register.inclusion_tag('django_privacy_mgmt/api.html')
def render_privacy_api():
    tracking_items = [item.data() for item in TrackingItem.objects.filter(site=settings.SITE_ID)]
    return {
        'tracking_items': tracking_items,
    }


@register.inclusion_tag('django_privacy_mgmt/link.html')
def render_privacy_settings_modal_link():
    pass


@register.inclusion_tag('django_privacy_mgmt/banner.html', takes_context=True)
def render_privacy_banner(context):
    sezikai_ctx_var = getattr(settings, 'SEKIZAI_VARNAME', 'SEKIZAI_CONTENT_HOLDER')
    tracking_items = TrackingItem.objects.filter(site=settings.SITE_ID)

    return {
        'essentials': tracking_items.filter(category=TRACKING_ITEM_CHOICE_ESSENTIALS),
        'statistics': tracking_items.filter(category=TRACKING_ITEM_CHOICE_STATISTICS),
        'marketing': tracking_items.filter(category=TRACKING_ITEM_CHOICE_MARKETING),
        'social_media': tracking_items.filter(category=TRACKING_ITEM_CHOICE_SOCIAL_MEDIA),
        'miscellaneous': tracking_items.filter(category=TRACKING_ITEM_CHOICE_MISCELLANEOUS),
        'tracking_items': tracking_items,
        sezikai_ctx_var: context.get(sezikai_ctx_var, defaultdict(UniqueSequence)),
    }

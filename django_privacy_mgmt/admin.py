# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models
from django.forms import Textarea
from django.utils.translation import gettext_lazy as _
from djangocms_text_ckeditor.widgets import TextEditorWidget
from parler.admin import (
    TranslatableAdmin,
    TranslatableStackedInline,
)
from solo.admin import SingletonModelAdmin

from .models import PrivacySettings, TrackingCookie, TrackingItem


class TrackingCookieInline(TranslatableStackedInline):
    model = TrackingCookie
    extra = 0
    formfield_overrides = {
        models.TextField: {
            'widget': Textarea(attrs={'style': 'width:100% !important;'}),
        },
    }


class TrackingItemAdmin(TranslatableAdmin):
    list_display = (
        '__str__',
        'category',
    )
    search_fields = ('name',)
    list_filter = ('category',)
    inlines = (TrackingCookieInline,)

    filter_vertical = ('site',)


class PrivacySettingsAdmin(TranslatableAdmin, SingletonModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': TextEditorWidget()},
    }
    fieldsets = (
        (
            None,
            {
                'fields': ('modal_description',),
            },
        ),
        (
            _('Category Essential'),
            {
                'fields': (
                    ('category_essential_title'),
                    ('category_essential_description'),
                ),
            },
        ),
        (
            _('Category Statistics'),
            {
                'fields': (
                    ('category_statistics_title'),
                    ('category_statistics_description'),
                ),
            },
        ),
        (
            _('Category Marketing'),
            {
                'fields': (
                    ('category_marketing_title'),
                    ('category_marketing_description'),
                ),
            },
        ),
        (
            _('Category Social Media'),
            {
                'fields': (
                    ('category_social_media_title'),
                    ('category_social_media_description'),
                ),
            },
        ),
        (
            _('Category Miscellaneous'),
            {
                'fields': (
                    ('category_miscellaneous_title'),
                    ('category_miscellaneous_description'),
                ),
            },
        ),
    )


admin.site.register(TrackingItem, TrackingItemAdmin)
admin.site.register(PrivacySettings, PrivacySettingsAdmin)

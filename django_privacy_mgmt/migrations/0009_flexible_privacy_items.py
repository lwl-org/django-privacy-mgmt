from django.db import migrations, models
from django.utils import translation
import django.db.models.deletion
import parler.fields
import parler.models

def forwards(apps, schema_editor):
    translation.activate('en')
    # Get tracking items
    TrackingItem = apps.get_model('django_privacy_mgmt', 'TrackingItem')
    old_items = TrackingItem.objects.all()

    # Create new tracking items
    essential_item = TrackingItem()
    essential_item.name = 'Essential Cookies'
    essential_item.slug = 'ESSENTIAL_COOKIES'
    essential_item.category = 'ESSENTIAL'
    essential_item.save()

    statistics_item = TrackingItem()
    statistics_item.name = 'Matomo'
    statistics_item.slug = 'MATOMO'
    statistics_item.category = 'STATISTICS'
    statistics_item.save()

    youtube_item = TrackingItem()
    youtube_item.name = 'Youtube Videos'
    youtube_item.slug = 'YOUTUBE_VIDEOS'
    youtube_item.category = 'SOCIAL_MEDIA'
    youtube_item.save()

    gmaps_item = TrackingItem()
    gmaps_item.name = 'Google Maps'
    gmaps_item.slug = 'GOOGLE_MAPS'
    gmaps_item.category = 'SOCIAL_MEDIA'
    gmaps_item.save()

    # Create new tracking cookies
    TrackingCookie = apps.get_model('django_privacy_mgmt', 'TrackingCookie')
    for old_item in old_items:
        cookie = TrackingCookie()
        if old_item.category == 'YOUTUBE_VIDEOS':
            cookie.item = youtube_item
        elif old_item.category == 'GOOGLE_MAPS':
            cookie.item = gmaps_item
        elif old_item.category == 'STATISTICS':
            cookie.item = statistics_item
        elif old_item.category == 'ESSENTIAL':
            cookie.item = essential_item
        else:
            cookie.item = essential_item
        for language in ['de', 'en']:
            old_item.set_current_language(language)

            try:
                cookie.set_current_language(language)
                cookie.name = old_item.name
                cookie.description = old_item.description
                cookie.lifetime = old_item.lifetime
                cookie.domain = old_item.domain
            except Exception as ex:
                print(ex)
        cookie.save()


class Migration(migrations.Migration):

    dependencies = [
        ('django_privacy_mgmt', '0008_auto_20221103_1302'),
    ]

    operations = [
        migrations.AddField(
            model_name='trackingitemtranslation',
            name='slug',
            field=models.SlugField(blank=True, max_length=100, verbose_name='Slug'),
        ),
        migrations.AddField(
            model_name='privacysettingstranslation',
            name='category_social_media_description',
            field=models.TextField(blank=True, default='', verbose_name='Category social media description'),
        ),
        migrations.AddField(
            model_name='privacysettingstranslation',
            name='category_social_media_title',
            field=models.CharField(blank=True, default='', max_length=1024, verbose_name='Category social media title'),
        ),
        migrations.AlterField(
            model_name='privacysettingstranslation',
            name='category_essential_title',
            field=models.CharField(blank=True, default='', max_length=1024, verbose_name='Category essential title'),
        ),
        migrations.AlterField(
            model_name='privacysettingstranslation',
            name='category_marketing_title',
            field=models.CharField(blank=True, default='', max_length=1024, verbose_name='Category marketing title'),
        ),
        migrations.AlterField(
            model_name='privacysettingstranslation',
            name='category_statistics_title',
            field=models.CharField(blank=True, default='', max_length=1024, verbose_name='Category statistics title'),
        ),
        migrations.AlterField(
            model_name='trackingitem',
            name='category',
            field=models.CharField(choices=[('ESSENTIAL', 'Essential'), ('STATISTICS', 'Statistics'), ('MARKETING', 'Marketing'), ('SOCIAL_MEDIA', 'Social Media'), ('MISCELLANEOUS', 'Miscellaneous')], default='ESSENTIAL', max_length=1024),
        ),
        migrations.AlterField(
            model_name='trackingitemtranslation',
            name='description',
            field=models.TextField(blank=True, help_text="What's the item's purpose?", null=True, verbose_name='Description'),
        ),
        migrations.CreateModel(
            name='TrackingCookie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ordering', models.IntegerField(default=0, verbose_name='Ordering')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cookies', to='django_privacy_mgmt.trackingitem')),
            ],
            options={
                'ordering': ('ordering',),
            },
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name='TrackingCookieTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('language_code', models.CharField(db_index=True, max_length=15, verbose_name='Language')),
                ('name', models.CharField(blank=True, max_length=1024, verbose_name='Name')),
                ('description', models.TextField(blank=True, help_text="What's the cookie's purpose?", null=True, verbose_name='Description')),
                ('lifetime', models.CharField(blank=True, help_text="The cookie's liftime (e.g. 1 Year)", max_length=255, null=True, verbose_name='Lifetime')),
                ('domain', models.CharField(blank=True, help_text='Domain of the cookie (e.g. .domain.com)', max_length=1024, null=True, verbose_name='Domain')),
                ('master', parler.fields.TranslationsForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='django_privacy_mgmt.trackingcookie')),
            ],
            options={
                'verbose_name': 'tracking cookie Translation',
                'db_table': 'django_privacy_mgmt_trackingcookie_translation',
                'db_tablespace': '',
                'managed': True,
                'default_permissions': (),
                'unique_together': {('language_code', 'master')},
            },
            bases=(parler.models.TranslatedFieldsModelMixin, models.Model),
        ),
        migrations.RunPython(forwards, migrations.RunPython.noop),

        migrations.RemoveField(
            model_name='trackingitemtranslation',
            name='domain',
        ),
        migrations.RemoveField(
            model_name='trackingitemtranslation',
            name='lifetime',
        ),
        migrations.RemoveField(
            model_name='privacysettingstranslation',
            name='category_google_maps_description',
        ),
        migrations.RemoveField(
            model_name='privacysettingstranslation',
            name='category_google_maps_title',
        ),
        migrations.RemoveField(
            model_name='privacysettingstranslation',
            name='category_youtube_videos_description',
        ),
        migrations.RemoveField(
            model_name='privacysettingstranslation',
            name='category_youtube_videos_title',
        ),
    ]

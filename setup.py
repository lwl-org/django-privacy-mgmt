# -*- coding: utf-8 -*-
from setuptools import find_packages, setup

from django_privacy_mgmt import __version__

setup(
    name='django-privacy-mgmt',
    version=__version__,
    description='This package provides a simple interface to provide GDPR-compliant cookie and tracking management on a website.',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    author='what.digital',
    author_email='mario@what.digital',
    packages=find_packages(),
    platforms=['OS Independent'],
    install_requires=[
        'django-parler>=1.8.1',
        'Django>=1.8',
        'django-sekizai>=0.10.0',
        'django-solo>=2',
        'djangocms_text_ckeditor>=4',
    ],
    download_url=f'https://gitlab.com/what-digital/django-privacy-mgmt/-/archive/{__version__}/django-privacy-mgmt-{__version__}.tar.gz',
    url='https://gitlab.com/what-digital/django-privacy-mgmt/tree/master',
    include_package_data=True,
    zip_safe=False,
)
